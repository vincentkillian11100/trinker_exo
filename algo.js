var colors = require('colors');
const people = require('./people');
const tr = require('./trinker');

console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.nbOfMale(people));
console.log("Nombre de femmes : ",                                                                  tr.nbOfFemale(people));
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nbOfMaleInterest(people));
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nbOfFemaleInterest(people));
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.nbOfmoney2000$(people));
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.nbOflikedrama(people));
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.Femalelikefiction(people));
console.log(tr.line('LEVEL 2'));
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",          tr.nbOflikeDocet1482(people));
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.nbofpren(people));
console.log("Homme le plus riche (nom et id) :",                                                    tr.Hommeplusriche(people));
console.log("Salaire moyen :",                                                                      tr.MoyenneSalaire(people));
console.log("Salaire médian :",                                                                     tr.MedianneSalaiefunction(people));
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                            tr.nbpershemisnord(people));
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                     tr.MoyenneSalHemisSud(people));
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.PersPlusProcheBener(people));
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.PersPlusProcheBrach(people));
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",                   "create function".blue);
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.Persworkgoogle(people));
console.log("Personne la plus agée :",                                                              tr.LeplsVieux(people));
console.log("Personne la plus jeune :",                                                             tr.LeplsJeune(people));
console.log("Moyenne des différences d'age :",                                                      tr.MoyenneDifAge(people));
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    tr.filmpluslook(people));
console.log("Genres de film par ordre de popularité :",                                             tr.filmpluslookParOrdre(people));
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  tr.ListGenreFilm(people));
console.log("Age moyen des hommes qui aiment les films noirs :",                                    tr.MoyenAgeHomFilmNoir(people));
console.log(`Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire 
de Paris et gagnent moins que la moyenne des hommes :`,                                                 "create function".blue);
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
préférence de film en commun (afficher les deux et la distance entre les deux):`,                   "create function".blue);
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            "create function".blue);
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));