const { slice, push, includes, sort } = require("./people");

module.exports = {
    title: function () {
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },
    //     1 niveau
    line: function (title = "=") {
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },
    //fonction 1 Nombre d'hommes
    allMale: function (p) {
        let x = [];
        for (let i of p) {
            if (i.gender === "Male") {
                x.push(i);
            }
        }
        return x;
    },
    //fonction 2 Nombre de femmes
    allFemale: function (p) {
        let x = [];
        for (let i of p) {
            if (i.gender === "Female") {
                x.push(i);
            }
        }
        return x;
    },
    // fonction 3 Nombre de personnes qui cherchent un homme 

    nbOfMale: function (p) {
        let i = this.allMale(p).length;
        return i;
    },
    // fonction 4 Nombre de personnes qui cherchent une femme 
    nbOfFemale: function (p) {
        let i = this.allFemale(p).length;
        return i;
    },
    
    nbOfmoney2000$: function (p) {
        let n = 0;
        for (let i of p) {
            resu = i.income;
            val = parseFloat(resu.slice(1));
            if (val > 2000) {
                n++
            }
        }
        return n;
    },

    nbOfMaleInterest: function (p) {
        let int = 0;
        for (let x of p) {
            if (x.looking_for === "M") int++
        }
        return int;
    },
    nbOfFemaleInterest: function (p) {
        let int = 0;
        for (let x of p) {
            if (x.looking_for === "F") int++
        }
        return int;
    },
    //fonction 7 Nombre de femmes qui aiment la science-fiction
    nbOflikedrama: function (p) {
        let int = 0;
        for (let x of p) {
            if (x.pref_movie.includes("Drama")) int++;
        }
        return int;
    },
    Femalelikefiction: function (p) {
        let n = 0;
        for (let x of p) {
            if (x.gender === 'Female' && x.pref_movie.includes('Sci-Fi')) {
                n++
            }
        };
        return n;
    },
    nbOflikeDocet1482: function (p) {
        let n = 0;
        for (let x of p) {
            resu = x.income;
            val = parseFloat(resu.slice(1));
            if (x.pref_movie === "Documentary" && val > 1482) {
                n++
            };
        };
      return n;
    },
    nbofpren: function (p) {
        let n = [];
        for (let x of p) {
            y = x.income.substring(1)
            if (y > 4000) {
                let k = [x.last_name, x.last_name, x.id, x.income]
                n.push(k);
            }
        }
        return n;
    },
    Hommeplusriche: function (p) {
        let crisuse = 0;
        let array = [];
        for (let x of this.allMale(p)) {
            if (parseFloat(x.income.substring(1)) > crisuse) {
                crisuse = x.income.substring(1);
                array = [x.last_name, x.id,];
            }
        }
        return `l'homme le plus riche est ${array}, sont salaire est de ${crisuse}$`;
    },
    // salaire moyen
    MoyenneSalaire: function (p) {
        let total = 0;
        for (let x of p) {
            money = parseFloat(x.income.substring(1));
            total += money;
        }
        return total / p.length;
    },

    MedianneSalaiefunction: function (p) {
        let les_salaires = []
        for (let x of p) {
            resu = x.income;
            val = parseFloat(resu.slice(1));
            les_salaires.push(val);
        }
        les_salaires = les_salaires.sort((a, b) => a - b);
        return les_salaires[les_salaires.length / 2]
    },
    AllPers_hemisSud: function (p) {
        let nordiste = []
        for (let pers of p) {
            lat = pers.latitude;
            if (lat < 0) {
                nordiste.push(pers)
            }
        }
        return nordiste
    },
    nbpershemisnord: function (p) {
        let nordiste = []
        for (let pers of p) {
            lat = pers.latitude;
            if (lat > 0) {
                nordiste.push(pers)
            }
        }
        return nordiste.length
    },
    MoyenneSalHemisSud: function (p) {
        let p_N = this.AllPers_hemisSud(p);
        return this.MoyenneSalaire(p_N)
    },
    //     2eme niveau

    //fonction 1 Personne qui habite le plus près de Bérénice Cawt (nom et id)
    //  PYTHAGORE

    pythagore: function (a, b) {
        let x = Math.sqrt(
            ((b.long - a.long) ** 2) +
            ((b.lat - a.lat) ** 2)
        );
        return x;
    },
    PersPlusProcheBener: function (p) {
        let a = { long: 0, lat: 0 }
        for (let x of p) {
            if (x.last_name == 'Cawt') {
                a.long = x.longitude;
                a.lat = x.latitude;
            }
        }
        let dist = 999999999999.0;
        let calcule = [];
        for (let i of p) {
            let b = { long: i.longitude, lat: i.latitude };
            if ((this.pythagore(a, b) < dist) && (this.pythagore(a, b) !== 0)) {
                dist = this.pythagore(a, b);
                calcule = [i.last_name, i.id];
            }
        }
        return calcule;
    },

    PersPlusProcheBrach: function (p) {
        let a = { long: 0, lat: 0 }
        for (let x of p) {
            if (x.last_name == 'Brach') {
                a.long = x.longitude;
                a.lat = x.latitude;
            }
        }
        let dist = 999999999999.0;
        let calcule = [];
        for (let i of p) {
            let b = { long: i.longitude, lat: i.latitude };
            if ((this.pythagore(a, b) < dist) && (this.pythagore(a, b) !== 0)) {
                dist = this.pythagore(a, b);
                calcule = [i.last_name, i.id];
            }
        }
        return calcule;
    },
  



    /*  ChercherPers: function (p, first_name, last_name) {
          for (pers of p) {
              if (pers.first_name === first_name && pers.last_name === last_name) {
                  pers_FN = pers;
                  break;
              }
          }
         return pers_FN;
      },
      // 1 recup furst name last name 
      // cree un tableau de personne avec leur distance
      //trier ce tableaau par orde croisaant
      //afficher les 10premier
      PersPlusProche10: function (p, first_name, last_name) {
          // 1 recup pers_FN furst name last name 
          let pers_FN = this.ChercherPers(p, first_name, last_name);
  
          // 2) et sa position xy_FN
          let xy_FN = { 
              long: pers_FN.longitude,
              lat: pers_FN.latitude
          };
  
          // 3) cree un tableau de personne avec leur distance
          let tab_pd= [];
          for (let pers of p) {
              // calcul la dist entre pd et pers_FN
                 let xy_pd = {
                     long: pers.longitude,
                     lat: pers.latitude
                 }
                 let dist = this.pythagore(xy_FN,xy_pd);
  
              // remplir tab tab_pd
               let pd = {
                   "pi": pers,
                   "d":  dist
              }
               tab_pd.push( [ pd ] )
          }
  
          // 4 trier le tableau 
          let tab2 = tab_pd.sort((pd1,pd2) => (pd1.d - pd2.d ));
          // 5 Afficher les 10 + proche
          for(i=0;i<10;i++){
             console.log(tab2[i]);
          }
         // return tab_pd[i]
         //personne la plus agée
  
  
  
      }, */
    Persworkgoogle: function (p) {
        //personne qui travaille a google
        let workgle = []
        for (let pers of p) {
            mail = pers.email;
            if (mail.includes("google")) {
                let k = [pers.first_name, pers.id]
                workgle.push(k);
            }
        }

        return workgle
    },
    LeplsVieux: function (p) {
        let tab = []
        for (let pers of p) {
            tab.push({
                nom: pers.last_name,
                anner: new Date(pers.date_of_birth)
            })
            tab.sort(function (a, b) { return a.anner - b.anner; })

        }
        return tab[0]
    },
    LeplsJeune: function (p) {
        let tab = []
        for (let pers of p) {
            tab.push({
                nom: pers.last_name,
                anner: new Date(pers.date_of_birth)
            })
            tab.sort(function (a, b) { return b.anner - a.anner; })

        }
        return tab[0]
    },

    MoyenneSalaire: function (p) {
        let total = 0;
        for (let x of p) {
            money = parseFloat(x.income.substring(1));
            total += money;
        }
        return total / p.length;
    },


    TrouverAge: function (p) {
        let diff = p.date_of_birth;
        let lage = new Date(diff);

        return ~~((Date.now() - lage) / 31557600000);
        // 24* 3600* 365.5* 1000
    },

    MoyenneDifAge: function (p) {

        let tabD = [];
        let comp = [];
        const reduire = (accumulator, curr) => accumulator + curr;
        for (persD of p) {
            let age = this.TrouverAge(persD);
            tabD.push(persD.age)
            for (persD2 of p) {
                age2 = this.TrouverAge(persD2);
                tabD.push(persD2.age2)
                if (persD > persD2) {
                    comp.push(age - age2)
                } else {
                    comp.push(age2 - age)
                }
            }
        }
        return comp.reduce(reduire) / comp.length;
    },

    filmpluslook: function (p) {
        drama = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Drama")) {
                c = drama.push(movie);
            }
        }
        Thriller = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Thriller")) {
                d = Thriller.push(movie);
            }
        }
        SciFi = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Sci-Fi")) {
                e = SciFi.push(movie);
            }
        }
        Action = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Action")) {
                f = Action.push(movie);
            }
        }
        Adventure = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Adventure")) {
                g = Adventure.push(movie);
            }
        }
        Horror = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Horror")) {
                h = Horror.push(movie);
            }
        }
        Crime = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Crime")) {
                i = Crime.push(movie);
            }
        }
        Mystery = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Mystery")) {
                j = Mystery.push(movie);
            }
        }
        Musical = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Musical")) {
                k = Musical.push(movie);
            }
        }
        Documentary = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Documentary")) {
                l = Documentary.push(movie);
            }
        }
        Fantasy = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Fantasy")) {
                m = Fantasy.push(movie);
            }
        }
        Children = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Children")) {
                n = Children.push(movie);
            }
        }
        Romance = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Romance")) {
                o = Romance.push(movie);
            }
        }
        Western = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Western")) {
                p = Western.push(movie);
            }
        }
        z = []
        z = 
            [`drama`,drama.length]


        return z
    },
    


       filmpluslookParOrdre: function (p) {
        drama = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Drama")) {
                c = drama.push(movie);
            }
        }
        Thriller = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Thriller")) {
                d = Thriller.push(movie);
            }
        }
        SciFi = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Sci-Fi")) {
                e = SciFi.push(movie);
            }
        }
        Action = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Action")) {
                f = Action.push(movie);
            }
        }
        Adventure = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Adventure")) {
                g = Adventure.push(movie);
            }
        }
        Horror = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Horror")) {
               i  = Horror.push(movie);
            }
        }
        Crime = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Crime")) {
               h  = Crime.push(movie);
            }
        }
        Mystery = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Mystery")) {
                j =  Mystery.push(movie);
            }
        }
        Musical = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Musical")) {
                k =  Musical.push(movie);
            }
        }
        Documentary = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Documentary")) {
                l =  Documentary.push(movie);
            }
        }
        Fantasy = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Fantasy")) {
                m =  Fantasy.push(movie);
            }
        }
        Children = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Children")) {
                n =  Children.push(movie);
            }
        }
        Romance = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Romance")) {
                o =  Romance.push(movie);
            }
        }
        Western = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Western")) {
                p =  Western.push(movie);
            }
        }
         
       z = []
       z = [drama.length,
            Romance.length,
            Thriller.length,
            Action.length,
            Documentary.length,
           Adventure.length,
           Horror.length,
           SciFi.length,
          Fantasy.length,
          Children.length,
           Mystery.length,
           Musical.length,
            Western.length]
            return z
    },
   ListGenreFilm: function (p) {
        drama = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Drama")) {
                c = drama.push(movie);
            }
        }
        Thriller = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Thriller")) {
                d = Thriller.push(movie);
            }
        }
        SciFi = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Sci-Fi")) {
                e = SciFi.push(movie);
            }
        }
        Action = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Action")) {
                f = Action.push(movie);
            }
        }
        Adventure = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Adventure")) {
                g = Adventure.push(movie);
            }
        }
        Horror = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Horror")) {
               i  = Horror.push(movie);
            }
        }
        Crime = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Crime")) {
               h  = Crime.push(movie);
            }
        }
        Mystery = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Mystery")) {
                j =  Mystery.push(movie);
            }
        }
        Musical = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Musical")) {
                k =  Musical.push(movie);
            }
        }
        Documentary = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Documentary")) {
                l =  Documentary.push(movie);
            }
        }
        Fantasy = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Fantasy")) {
                m =  Fantasy.push(movie);
            }
        }
        Children = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Children")) {
                n =  Children.push(movie);
            }
        }
        Romance = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Romance")) {
                o =  Romance.push(movie);
            }
        }
        Western = []
        for (let pers of p) {
            movie = pers.pref_movie
            if (movie.includes("Western")) {
                p =  Western.push(movie);
            }
        }
         
       z = []
       z = [`drama`,drama.length,
       `Romance`,Romance.length,
       `Thiller`,Thriller.length,
      `Action`, Action.length,
       `Documentary`,Documentary.length,
      `adventure`, Adventure.length,
     `horror`,  Horror.length,
     `science-fiction`,  SciFi.length,
     `Fantasy`,  Fantasy.length,
      `Children`, Children.length,
      `Mystery`, Mystery.length,
      `Musical`, Musical.length,
      `Western`, Western.length]
            return z
    },

           MoyenAgeHomFilmNoir: function (p) {
            total = [];
            const reducer = (previousValue, currentValue) =>
              previousValue + currentValue;
            for (let i of this.allMale(p)) {
              if (i.pref_movie.includes("Film-Noir")) {
                total.push(this.TrouverAge(i));
              }
            }
          
            return total.reduce(reducer) / total.length;
          },
    


    match: function (p) {
        return "not implemented".red;
    }
}